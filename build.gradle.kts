import com.github.benmanes.gradle.versions.reporter.PlainTextReporter
import com.github.benmanes.gradle.versions.reporter.result.Result
import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.extensions.DetektExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.owasp.dependencycheck.gradle.extension.AnalyzerExtension

plugins {
  kotlin("jvm") version "1.8.10"

  id("org.springframework.boot") version "2.7.5"
  id("gg.jte.gradle") version "2.2.4"
  id("com.google.protobuf") version "0.9.2"

  id("io.spring.dependency-management") version "1.1.0"
  id("com.gorylenko.gradle-git-properties") version "2.4.1"

  id("org.jetbrains.kotlinx.kover") version "0.6.1"

  id("org.jmailen.kotlinter") version "3.13.0"
  id("io.gitlab.arturbosch.detekt") version "1.22.0"

  id("org.owasp.dependencycheck") version "8.0.2"

  id("com.github.ben-manes.versions") version "0.45.0"
}

group = "com.stemmildt"
version = "0.0.1"

repositories {
  maven("https://repo.spring.io/milestone/")
  mavenCentral()
}

dependencyManagement {
  imports {
    mavenBom("org.testcontainers:testcontainers-bom:1.17.6")
    mavenBom("com.google.protobuf:protobuf-bom:3.21.12")
    mavenBom("io.arrow-kt:arrow-stack:1.1.5")
  }
}

dependencies {
  implementation("org.jetbrains.kotlin:kotlin-stdlib")
  implementation("org.jetbrains.kotlin:kotlin-reflect")
  implementation("io.projectreactor.kotlin:reactor-kotlin-extensions:1.2.1")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.6.4")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.14.2")

  implementation("org.jmolecules:jmolecules-ddd:1.6.0")
  implementation("io.arrow-kt:arrow-core")
  implementation("io.arrow-kt:arrow-fx-coroutines")

  // TODO LATER add stuff for fault tolerance
  // TODO LATER add jobs
  // TODO LATER add logging in elastic search compatible format

  // TODO LATER add toggles

  implementation("org.unbescape:unbescape:1.1.6.RELEASE")
  implementation("com.googlecode.owasp-java-html-sanitizer:owasp-java-html-sanitizer:20220608.1")
  implementation("com.github.slugify:slugify:3.0.2")

  implementation("io.github.microutils:kotlin-logging-jvm:3.0.5")

  // implementation("org.springdoc:springdoc-openapi-webflux-ui:1.6.9")
  // implementation("org.springdoc:springdoc-openapi-kotlin:1.6.9")

  api("com.google.protobuf:protobuf-java")
  // implementation("de.codecentric:spring-boot-admin-starter-client:2.7.0")
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("gg.jte:jte:2.2.4")
  implementation("org.springframework.boot:spring-boot-starter-data-mongodb-reactive")
  implementation("org.springframework.kafka:spring-kafka:2.9.2")
  // implementation("org.springframework.boot:spring-boot-starter-actuator")

  implementation("org.springframework.fu:spring-fu-kofu:0.5.1")

  // TODO LATER activate with kofu is it still working?
  runtimeOnly("io.micrometer:micrometer-registry-prometheus:1.10.3")

  // TODO LATER not working for jte templates (see guide)
  developmentOnly("org.springframework.boot:spring-boot-devtools")
  developmentOnly("io.projectreactor:reactor-tools:3.5.2")

  testImplementation("com.tngtech.archunit:archunit-junit5:1.0.1")
  testImplementation("io.mockk:mockk:1.13.4")
  testImplementation("io.strikt:strikt-core:0.34.1")
  testImplementation("org.testcontainers:junit-jupiter")
  testImplementation("org.springframework.boot:spring-boot-starter-test") {
    exclude("org.junit.vintage")
  }
  testImplementation("io.projectreactor:reactor-test:3.5.2")
  testImplementation("org.springframework.kafka:spring-kafka-test:2.9.2")

  testImplementation("org.testcontainers:kafka")
  testImplementation("org.testcontainers:mongodb")

  detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.22.0")

  annotationProcessor("org.springframework:spring-context-indexer:6.0.4")
}

jte {
  generate()
}

sourceSets.main {
  java.setSrcDirs(listOf("src/main/java", "build/generated/source/proto/main/java", "build/generated-sources/jte"))
}

configure<DetektExtension> {
  config = files("$rootDir/detekt-config.yml")
}

dependencyCheck {
  analyzers(closureOf<AnalyzerExtension> {
    assemblyEnabled = false
    failBuildOnCVSS = 5F
    failOnError = true
    suppressionFile = "./gradle/config/suppressions.xml"
  })
}

java {
  sourceCompatibility = JavaVersion.VERSION_19
  targetCompatibility = JavaVersion.VERSION_19
}

tasks {
  withType<Detekt> {
    jvmTarget = JavaVersion.VERSION_19.toString()
  }

  withType<KotlinCompile> {
    kotlinOptions {
      useK2 = true
      jvmTarget = JavaVersion.VERSION_19.toString()
      apiVersion = "1.9"
      languageVersion = "1.9"
      javaParameters = true
      freeCompilerArgs = listOf("-Xjsr305=strict")
    }
    dependsOn("generateProto")
  }

  withType<Test> {
    useJUnitPlatform()
  }

  withType<DependencyUpdatesTask> {
    fun isStable(version: String): Boolean {
      val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.toUpperCase().contains(it) }
      val regex = "^[0-9,.v-]+(-r)?$".toRegex()
      return stableKeyword || regex.matches(version)
    }
    rejectVersionIf {
      isStable(currentVersion) && !isStable(candidate.version)
    }
    outputFormatter = closureOf<Result> {
      PlainTextReporter(project, System.getProperty("revision")
        ?: "milestone", gradleReleaseChannel).write(System.out, this)
      if (this.outdated.dependencies.isNotEmpty()) {
        throw GradleException("Abort, there are dependencies to update.")
      }
    }
  }
}
