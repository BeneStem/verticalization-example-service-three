package com.stemmildt.decide

import com.stemmildt.decide.DecideServiceConfiguration.decideServiceApplication

object DecideService

fun main(args: Array<String>) {
  decideServiceApplication.run(args)
}
