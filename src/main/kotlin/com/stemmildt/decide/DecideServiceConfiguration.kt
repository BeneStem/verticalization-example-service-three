package com.stemmildt.decide

import com.stemmildt.decide.adapter.outbound.OutboundAdapterConfiguration
import com.stemmildt.decide.application.ApplicationConfiguration
import org.springframework.fu.kofu.reactiveWebApplication

object DecideServiceConfiguration {

  val decideServiceApplication = reactiveWebApplication {
    enable(com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration())
    enable(OutboundAdapterConfiguration())
    enable(ApplicationConfiguration())
  }
}
