package com.stemmildt.decide.adapter.inbound

import com.stemmildt.decide.adapter.inbound.messaging.KafkaConsumerConfiguration
import com.stemmildt.decide.adapter.inbound.messaging.KafkaProductSavedEventListener
import com.stemmildt.decide.adapter.inbound.web.BasketPageController
import com.stemmildt.decide.adapter.inbound.web.BasketProductsCountController
import com.stemmildt.decide.adapter.inbound.web.JteConfiguration
import com.stemmildt.decide.adapter.inbound.web.MiniBasketSsiController
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.core.env.getRequiredProperty
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.webflux.webFlux

object InboundAdapterConfiguration {

  const val DECIDE_PATH: String = "/decide"

  operator fun invoke() = configuration {
    enable(webConfiguration())
    enable(messagingConfiguration())
  }

  private fun webConfiguration() = configuration {
    webFlux {
      port = env.getRequiredProperty<Int>("server.port")
      codecs {
        string()
      }
    }
    beans {
      bean(JteConfiguration::templateEngine)

      bean<BasketPageController>()
      bean(BasketPageController::routes)

      bean<MiniBasketSsiController>()
      bean(MiniBasketSsiController::routes)

      bean<BasketProductsCountController>()
      bean(BasketProductsCountController::routes)
    }
  }

  private fun messagingConfiguration() = configuration {
    beans {
      configurationProperties<KafkaProperties>(prefix = "spring.kafka")
      bean<KafkaConsumerConfiguration>()
      bean(KafkaConsumerConfiguration::listenerFactory)
      bean<KafkaProductSavedEventListener>()
      bean(KafkaProductSavedEventListener::create)
    }
  }
}
