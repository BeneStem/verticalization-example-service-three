package com.stemmildt.decide.adapter.inbound.messaging.model

sealed class KafkaError {

  data object UnknownKafkaProductEventType : KafkaError()
  data object UnknownKafkaProductSavedEventVersion : KafkaError()
}
