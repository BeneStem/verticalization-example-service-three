package com.stemmildt.decide.adapter.inbound.web

import com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration.DECIDE_PATH
import com.stemmildt.decide.adapter.inbound.web.MiniBasketSsiController.Companion.BASKET_ID_COOKIE_NAME
import com.stemmildt.decide.application.port.inbound.BasketApplicationService
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import com.stemmildt.decide.domain.model.web.Basket
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import gg.jte.Content
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class BasketPageController(
  private val templateEngine: TemplateEngine,
  private val basketApplicationService: BasketApplicationService
) {

  companion object {

    const val BASKET_PATH: String = "/basket"
  }

  fun routes() = coRouter {
    accept(TEXT_HTML).nest {
      GET(DECIDE_PATH + BASKET_PATH, ::getBasketPage)
    }
  }

  private suspend fun getBasketPage(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      val basketId = serverRequest.cookies().getFirst(BASKET_ID_COOKIE_NAME)!!.value
      val mainMap = mapOf("basket" to findBasket(basketId))
      templateEngine.render(
        "templates/pageFrame",
        mapOf(
          "head" to Content { templateEngine.render("fragments/head", mapOf("title" to "BasketPage"), it) },
          "main" to Content { templateEngine.render("fragments/basket", mainMap, it) }
        ),
        it
      )
      ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
    }

  private suspend fun findBasket(id: String?) =
    id?.let {
      BasketId.of(it).fold(
        { Basket("invalid id", listOf("")) },
        { findBasket(it) }
      )
    } ?: Basket("invalid id", listOf(""))

  private fun findBasket(validatedId: BasketId) =
    basketApplicationService.loadBasketById(validatedId).fold(
      { Basket("error", listOf("")) },
      {
        it.fold(
          { Basket("not found error", listOf("")) },
          { Basket(it.id.value, it.productIds.map { it.value.toString() }) }
        )
      }
    )
}
