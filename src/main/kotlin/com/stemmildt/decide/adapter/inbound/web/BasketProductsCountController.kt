package com.stemmildt.decide.adapter.inbound.web

import com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration.DECIDE_PATH
import com.stemmildt.decide.adapter.inbound.web.BasketPageController.Companion.BASKET_PATH
import com.stemmildt.decide.adapter.inbound.web.MiniBasketSsiController.Companion.BASKET_ID_COOKIE_NAME
import com.stemmildt.decide.application.port.inbound.BasketApplicationService
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import com.stemmildt.util.TextEventStream.toTextEventStreamDataString
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.http.MediaType.TEXT_EVENT_STREAM
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class BasketProductsCountController(
  private val templateEngine: TemplateEngine,
  private val basketApplicationService: BasketApplicationService
) {

  fun routes() = coRouter {
    accept(TEXT_EVENT_STREAM).nest {
      GET("$DECIDE_PATH$BASKET_PATH/products/count", ::getBasketProductsCount)
    }
  }

  private suspend fun getBasketProductsCount(serverRequest: ServerRequest): EntityResponse<Flow<String>> =
    basketApplicationService.streamProductCount(serverRequest.cookies().getFirst(BASKET_ID_COOKIE_NAME)!!.value)
      .map {
        StringOutput().let { output ->
          templateEngine.render("fragments/basketProductsCount", mapOf("basketProductsCount" to it), output)
          output.toTextEventStreamDataString()
        }
      }
      .let {
        ok(it).contentType(TEXT_EVENT_STREAM).buildAndAwait()
      }
}
