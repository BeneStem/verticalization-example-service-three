package com.stemmildt.decide.adapter.inbound.web

import com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration.DECIDE_PATH
import com.stemmildt.decide.application.port.inbound.BasketApplicationService
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import com.stemmildt.decide.domain.model.web.Basket
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class MiniBasketSsiController(
  private val templateEngine: TemplateEngine,
  private val basketApplicationService: BasketApplicationService
) {

  companion object {

    const val BASKET_ID_COOKIE_NAME = "cid"

    const val MINI_BASKET_PATH: String = "/mini-basket"
  }

  fun routes() = coRouter {
    accept(TEXT_HTML).and(path("/ssi")).nest {
      GET(DECIDE_PATH + MINI_BASKET_PATH, ::getMiniBasketSsi)
    }
  }

  private suspend fun getMiniBasketSsi(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      val basketId = serverRequest.cookies().getFirst(BASKET_ID_COOKIE_NAME)!!.value
      templateEngine.render("ssi/miniBasket", mapOf("basketProductsCount" to findBasket(basketId).productTiles.size.toLong()), it)
      ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
    }

  private suspend fun findBasket(id: String?) =
    id?.let {
      BasketId.of(it).fold(
        { Basket("invalid id", emptyList()) },
        { findBasket(it) }
      )
    } ?: Basket("invalid id", emptyList())

  private suspend fun findBasket(validatedId: BasketId) =
    basketApplicationService.loadBasketById(validatedId).fold(
      { Basket("error", emptyList()) },
      {
        it.fold(
          { Basket("not found error", emptyList()) },
          { Basket(it.id.value, it.productIds.map { it.value.toString() }) }
        )
      }
    )
}
