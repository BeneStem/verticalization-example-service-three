package com.stemmildt.decide.adapter.outbound.messaging.flow

import com.stemmildt.decide.application.port.outbound.ProductSavedEventForwarder
import com.stemmildt.decide.application.port.outbound.ProductSavedEventSubscriber
import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent
import com.stemmildt.util.SubscriptionFlow

class FlowProductSavedEventForwarder : ProductSavedEventForwarder, ProductSavedEventSubscriber {

  private val subscriptionFlow = SubscriptionFlow<ProductSavedEvent>()

  override fun forward(productSavedEvent: ProductSavedEvent) = subscriptionFlow.send(productSavedEvent)

  // TODO NOW use basketId
  override fun subscribe(basketId: String) = subscriptionFlow.getFlowFromHere()
}
