package com.stemmildt.decide.adapter.outbound.persistence.mongo

import arrow.core.Option
import com.stemmildt.decide.adapter.outbound.persistence.mongo.model.MongoBasket
import com.stemmildt.decide.application.port.outbound.BasketRepository
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import kotlinx.coroutines.runBlocking
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations
import org.springframework.data.mongodb.core.awaitOneOrNull
import org.springframework.data.mongodb.core.query
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query.query
import org.springframework.data.mongodb.core.query.isEqualTo

class MongoBasketRepository(
  private val mongo: ReactiveFluentMongoOperations
) : BasketRepository {

  override fun findBasketById(id: BasketId) =
    runBlocking {
      Option.fromNullable(
        mongo.query<MongoBasket>()
          .matching(query(where(MongoBasket::id.name).isEqualTo(id.value)))
          .awaitOneOrNull()
          ?.toBasket()
      )
    }
}
