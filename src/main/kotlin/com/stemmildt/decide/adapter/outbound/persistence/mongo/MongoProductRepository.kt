package com.stemmildt.decide.adapter.outbound.persistence.mongo

import arrow.core.invalidNel
import com.stemmildt.decide.adapter.outbound.persistence.mongo.model.MongoProduct
import com.stemmildt.decide.adapter.outbound.persistence.mongo.model.MongoProduct.Companion.toMongoProduct
import com.stemmildt.decide.adapter.outbound.persistence.mongo.model.MongoProductError
import com.stemmildt.decide.application.port.outbound.ProductRepository
import com.stemmildt.decide.domain.model.product.`object`.Product
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations
import org.springframework.data.mongodb.core.insert
import org.springframework.data.mongodb.core.oneAndAwait

class MongoProductRepository(
  private val mongo: ReactiveFluentMongoOperations
) : ProductRepository {

  override suspend fun persistProduct(product: Product) =
    try {
      mongo.insert<MongoProduct>()
        .oneAndAwait(product.toMongoProduct())
        .toProduct()
    } catch (duplicateKeyException: DuplicateKeyException) {
      MongoProductError.DuplicateKey.invalidNel()
    }
}
