package com.stemmildt.decide.adapter.outbound.persistence.mongo.model

import arrow.core.andThen
import arrow.core.invalidNel
import arrow.core.validNel
import com.stemmildt.decide.adapter.outbound.persistence.mongo.model.MongoProductError.IdNull
import com.stemmildt.decide.domain.model.basket.`object`.Basket
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import com.stemmildt.decide.domain.model.product.`object`.ProductId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "baskets")
data class MongoBasket(
  @Id
  val id: String?,
  val productIds: List<String>
) {

  companion object {

    fun Basket.toMongoBasket() = MongoBasket(id.value, productIds.map { it.value.toString() })
  }

  private fun validateId() = id?.validNel()
    ?: IdNull.invalidNel()

  fun toBasket() =
    validateId()
      .andThen(BasketId::of)
      .andThen {
        Basket.of(it, productIds.map { it.toInt() }.map(ProductId::invoke))
      }
}
