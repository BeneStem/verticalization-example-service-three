package com.stemmildt.decide.adapter.outbound.persistence.mongo.model

import com.stemmildt.util.Error

sealed class MongoProductError : Error {

  data object DuplicateKey : MongoProductError()
  data object IdNull : MongoProductError()
  data object DescriptionNull : MongoProductError()
}
