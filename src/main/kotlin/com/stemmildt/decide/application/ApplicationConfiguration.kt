package com.stemmildt.decide.application

import org.springframework.fu.kofu.configuration

object ApplicationConfiguration {

  operator fun invoke() = configuration {
    beans {
      bean<BasketApplicationService>()
      bean<ProductSavedEventApplicationService>()
    }
  }
}
