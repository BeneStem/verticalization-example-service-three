package com.stemmildt.decide.application

import com.stemmildt.decide.application.port.inbound.BasketApplicationService
import com.stemmildt.decide.application.port.outbound.BasketRepository
import com.stemmildt.decide.application.port.outbound.ProductSavedEventSubscriber
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import kotlinx.coroutines.flow.Flow

class BasketApplicationService(
  private val basketRepository: BasketRepository,
  private val productSavedEventSubscriber: ProductSavedEventSubscriber
) : BasketApplicationService {

  override fun loadBasketById(id: BasketId) =
    basketRepository.findBasketById(id)

  override suspend fun streamProductCount(basketId: String): Flow<Long> = TODO()
//    productSavedEventSubscriber.subscribe(id)
//      .flatMapConcat { flowOf(basketRepository.countProducts(basketId)) }

  // TODO MIKE
  // addToBasket() MONADE
}
