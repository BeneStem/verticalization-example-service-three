package com.stemmildt.decide.application

import com.stemmildt.decide.application.port.inbound.ProductSavedEventApplicationService
import com.stemmildt.decide.application.port.outbound.ProductRepository
import com.stemmildt.decide.application.port.outbound.ProductSavedEventForwarder
import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent
import kotlinx.coroutines.runBlocking

class ProductSavedEventApplicationService(
  private val productRepository: ProductRepository,
  private val productSavedEventForwarder: ProductSavedEventForwarder
) : ProductSavedEventApplicationService {

  override fun handle(productSavedEvent: ProductSavedEvent) =
    runBlocking {
      productRepository.persistProduct(productSavedEvent.product).tap {
        productSavedEventForwarder.forward(productSavedEvent)
      }
      Unit
    }
}
