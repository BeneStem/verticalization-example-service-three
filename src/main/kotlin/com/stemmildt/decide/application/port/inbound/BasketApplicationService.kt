package com.stemmildt.decide.application.port.inbound

import arrow.core.Option
import arrow.core.ValidatedNel
import com.stemmildt.decide.domain.model.basket.`object`.Basket
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import com.stemmildt.util.Error
import kotlinx.coroutines.flow.Flow

interface BasketApplicationService {

  fun loadBasketById(id: BasketId): Option<ValidatedNel<Error, Basket>>

  suspend fun streamProductCount(basketId: String): Flow<Long>
}
