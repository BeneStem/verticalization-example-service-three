package com.stemmildt.decide.application.port.inbound

import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent

interface ProductSavedEventApplicationService {

  fun handle(productSavedEvent: ProductSavedEvent)
}
