package com.stemmildt.decide.application.port.outbound

import arrow.core.Option
import arrow.core.ValidatedNel
import com.stemmildt.decide.domain.model.basket.`object`.Basket
import com.stemmildt.decide.domain.model.basket.`object`.BasketId
import com.stemmildt.util.Error

interface BasketRepository {

  fun findBasketById(id: BasketId): Option<ValidatedNel<Error, Basket>>
}
