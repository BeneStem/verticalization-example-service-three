package com.stemmildt.decide.application.port.outbound

import arrow.core.ValidatedNel
import com.stemmildt.decide.domain.model.product.`object`.Product
import com.stemmildt.util.Error

interface ProductRepository {

  suspend fun persistProduct(product: Product): ValidatedNel<Error, Product>
}
