package com.stemmildt.decide.application.port.outbound

import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent

interface ProductSavedEventForwarder {

  fun forward(productSavedEvent: ProductSavedEvent)
}
