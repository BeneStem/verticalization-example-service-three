package com.stemmildt.decide.application.port.outbound

import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent
import kotlinx.coroutines.flow.Flow

interface ProductSavedEventSubscriber {

  fun subscribe(basketId: String): Flow<ProductSavedEvent>
}
