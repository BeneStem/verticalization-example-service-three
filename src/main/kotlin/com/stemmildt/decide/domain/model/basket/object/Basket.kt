package com.stemmildt.decide.domain.model.basket.`object`

import arrow.core.validNel
import com.stemmildt.decide.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation
import com.stemmildt.extensions.Validation.get
import org.jmolecules.ddd.annotation.AggregateRoot

@AggregateRoot
class Basket private constructor(
  val id: BasketId,
  val productIds: List<ProductId>
) {

  companion object {
    fun of(id: BasketId, productIds: List<ProductId>) =
      Basket(id, productIds).validNel()

    @Throws(Validation.ValidationException::class)
    operator fun invoke(id: BasketId, productIds: List<ProductId>) =
      of(id, productIds).get()
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is Basket) return false

    return id == other.id
  }

  override fun hashCode() = id.hashCode()

  override fun toString() = "Basket(id=$id, productIds=$productIds)"
}
