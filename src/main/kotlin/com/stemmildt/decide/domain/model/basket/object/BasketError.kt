package com.stemmildt.decide.domain.model.basket.`object`

import com.stemmildt.util.Error

sealed class BasketError private constructor() : Error {

  data object IdInvalid : BasketError()
}
