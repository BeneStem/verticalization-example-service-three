package com.stemmildt.decide.domain.model.basket.`object`

import arrow.core.ValidatedNel
import arrow.core.validNel
import com.stemmildt.extensions.Validation.ValidationException
import com.stemmildt.extensions.Validation.get
import org.jmolecules.ddd.annotation.ValueObject

@ValueObject
@JvmInline
value class BasketId private constructor(val value: String) {

  companion object {

    @Throws(ValidationException::class)
    operator fun invoke(value: String) =
      of(value).get()

    // TODO add validation for: d13c444acb4d289333762c4a0c9138a5
    fun of(value: String): ValidatedNel<BasketError, BasketId> =
      BasketId(value).validNel()
  }
}
