package com.stemmildt.decide.domain.model.web

import org.jmolecules.ddd.annotation.AggregateRoot

// TODO NOW this domain structure is not optimal

@AggregateRoot
class Basket(
  val id: String,
  val productTiles: List<String>
) {

  companion object {

    operator fun invoke(id: String, productTiles: List<String>) = Basket(id, productTiles)
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is Basket) return false

    if (productTiles != other.productTiles) return false

    return true
  }

  override fun hashCode() = productTiles.hashCode()

  override fun toString() = "Basket(productTiles=$productTiles)"
}
