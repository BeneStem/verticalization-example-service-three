package com.stemmildt.decide.domain.model.web

import com.stemmildt.decide.domain.model.product.`object`.Product
import org.jmolecules.ddd.annotation.ValueObject

// TODO NOW handle error states

@ValueObject
data class ProductTile private constructor(
  val headline: String,
  val text: String,
  val link: String
) {

  companion object {

    operator fun invoke(product: Product) =
      ProductTile(
        product.description.value,
        product.id.value.toString(),
        "/evaluate/products/${product.id.value}"
      )
  }
}
