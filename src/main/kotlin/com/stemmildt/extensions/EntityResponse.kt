package com.stemmildt.extensions

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.OK
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.EntityResponse.fromObject
import org.springframework.web.reactive.function.server.EntityResponse.fromProducer

object EntityResponse {

  fun <T : Any> ok(body: T) = fromObject(body).status(OK)
  fun <T : Any> created(body: T) = fromObject(body).status(CREATED)
  fun <T : Any> notFound(body: T) = fromObject(body).status(NOT_FOUND)
  fun <T : Any> badRequest(body: T) = fromObject(body).status(BAD_REQUEST)

  inline fun <T : Flow<E>, reified E : Any> ok(producer: T) = fromProducer(producer, E::class.java).status(OK)

  suspend fun <T> EntityResponse.Builder<T>.buildAndAwait(): EntityResponse<T> = this.build().awaitSingle()
}
