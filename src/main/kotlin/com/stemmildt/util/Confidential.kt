package com.stemmildt.util

annotation class Confidential(val description: String)
