package com.stemmildt.util

import org.owasp.html.HtmlPolicyBuilder
import org.unbescape.html.HtmlEscape

object Sanitizing {

  private val DISALLOW_ANYTHING_POLICY = HtmlPolicyBuilder().toFactory()

  private fun sanitizeString(text: String): String = unescapeAgainBecauseSanitizingEscapesText(unescapeAndSanitize(text)).trim()

  private fun unescapeAndSanitize(text: String) = DISALLOW_ANYTHING_POLICY.sanitize(HtmlEscape.unescapeHtml(text))

  private fun unescapeAgainBecauseSanitizingEscapesText(text: String) = HtmlEscape.unescapeHtml(text)

  fun String.sanitize() = sanitizeString(this)
}
