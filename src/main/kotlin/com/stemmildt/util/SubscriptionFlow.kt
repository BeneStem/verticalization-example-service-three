package com.stemmildt.util

import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SubscriptionFlow<A> {

  private data class Cons<A>(val first: A, val deferred: CompletableDeferred<Cons<A>> = CompletableDeferred())

  private var currentDeferred = CompletableDeferred<Cons<A>>()

  @OptIn(ExperimentalCoroutinesApi::class)
  fun send(element: A) {
    currentDeferred.complete(Cons(element))
    currentDeferred = currentDeferred.getCompleted().deferred
  }

  fun getFlowFromHere(): Flow<A> {
    var current = currentDeferred
    return flow {
      while (true) {
        val next = current.await()
        emit(next.first)
        current = next.deferred
      }
    }
  }
}
