package com.stemmildt.decide.adapter.inbound.web

import arrow.core.andThen
import com.stemmildt.decide.IntegrationTest.Companion.serviceContext
import com.stemmildt.decide.IntegrationTest.Companion.webTestClient
import com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration.DECIDE_PATH
import com.stemmildt.decide.adapter.inbound.web.BasketPageController.Companion.BASKET_PATH
import com.stemmildt.decide.application.BasketApplicationService
import com.stemmildt.decide.domain.model.messaging.ProductSavedEvent
import com.stemmildt.decide.domain.model.product.`object`.Product
import com.stemmildt.decide.domain.model.product.`object`.ProductDescription
import com.stemmildt.decide.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation.validate
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import strikt.api.expectThat
import strikt.assertions.contains

class BasketPageControllerIntegrationTest : com.stemmildt.decide.IntegrationTest {

  private val productApplicationService = serviceContext.getBean(BasketApplicationService::class.java)

  @Test
  fun expectToGetProductDetailPage() {
    val productDescriptionValue = "Pizza"

    validate(
      ProductId.of(1),
      ProductDescription.of(productDescriptionValue)
    ).andThen { (id, description) ->
      Product.of(id, description).andThen(ProductSavedEvent::of)
    }.tap {
      // TODO NOW
      // runBlocking { productApplicationService.publishProductSavedEvent(it) }
    }

    webTestClient.get()
      .uri(DECIDE_PATH + BASKET_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}
