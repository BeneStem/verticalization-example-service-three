package com.stemmildt.decide.adapter.inbound.web

import com.stemmildt.decide.adapter.inbound.InboundAdapterConfiguration.DECIDE_PATH
import com.stemmildt.decide.adapter.inbound.web.BasketPageController.Companion.BASKET_PATH
import com.stemmildt.decide.application.port.inbound.BasketApplicationService
import com.stemmildt.decide.domain.model.product.`object`.Product
import com.stemmildt.decide.domain.model.product.`object`.ProductDescription
import com.stemmildt.decide.domain.model.product.`object`.ProductId
import gg.jte.ContentType
import gg.jte.TemplateEngine
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.test.web.reactive.server.WebTestClient
import strikt.api.expectThat
import strikt.assertions.contains

class BasketPageControllerUnitTest {

  private val basketApplicationServiceMock = mockk<BasketApplicationService>()
  private val productDetailPageController = BasketPageController(TemplateEngine.createPrecompiled(ContentType.Html), basketApplicationServiceMock)

  private val webTestClient = WebTestClient.bindToRouterFunction(productDetailPageController.routes()).build()

  @Test
  fun expectToGetProductDetailPage() {
    val productDescriptionValue = "Pizza"

    val pizza = Product(ProductId(1), ProductDescription(productDescriptionValue))
    // TODO NOW
//    every { runBlocking { productApplicationServiceMock.loadProductCount() } } returns 1L
//    every { runBlocking { productApplicationServiceMock.loadAllProducts() } } returns listOf(pizza.validNel()).toFlux().asFlow()

    webTestClient.get()
      .uri(DECIDE_PATH + BASKET_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}
